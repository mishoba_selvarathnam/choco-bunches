<?php
session_start();
if(isset($_SESSION["uid"])){
	header("location:profile.php");
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Mobile World</title>
		<link rel="stylesheet" href="css/bootstrap.min.css"/>
		<script src="js/jquery2.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="main.js"></script>
		<!-- <script src="js/jquery.min.js"></script> -->
		<script src="js/responsiveslides.min.js"></script>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link rel="stylesheet" href="css/responsiveslides.css">
		<link href="css/style.css" rel="stylesheet" type="text/css"  media="all" />
		<link href='//fonts.googleapis.com/css?family=Londrina+Solid|Coda+Caption:800|Open+Sans' rel='stylesheet' type='text/css'>
		<style></style>
		<script>
		    // You can also use "$(window).load(function() {"
			    $(function () {
			
			      // Slideshow 1
			      $("#slider1").responsiveSlides({
			        maxwidth: 1600,
			        speed: 600
			      });
			});
		  </script>
	</head>
<body>
<div class="wait overlay">
	<div class="loader"></div>
</div>
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="wrap">
		<!----start-Header---->
		<div class="header">
			<div class="search-bar">
				<form>
				<div >
					<input type="text"><input type="submit" id="search_btn" value="Search" />
					</div>					
				</form>
			</div>		
			<div class="clear"> </div>
		</div>
	</div>
		<div class="container-fluid">	
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse" aria-expanded="false">
					<span class="sr-only">navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="logo">
				<a href="index.php"><img src="product_images/logo.png" title="logo" / style="width:100px; height:50px;"></a>
			</div>
			</div>
		<div class="collapse navbar-collapse" id="collapse">
			<ul class="nav navbar-nav">
				<li><a href="index.php"><span class="glyphicon glyphicon-home"></span>Home</a></li>
				<li><a href="index.php"><span class="glyphicon glyphicon-modal-window"></span>Product</a></li>
				<li><a href="contactus.php"><span class="glyphicon glyphicon-modal-window"></span>Contact Us</a></li>
				<li><a href="aboutus.php"><span class="glyphicon glyphicon-modal-window"></span>About Us</a></li>
			</ul>
						
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span>Cart<span class="badge">0</span></a>
					<div class="dropdown-menu" style="width:400px;">
						<div class="panel panel-success">
							<div class="panel-heading">
								<div class="row">
									<div class="col-md-3">Sl.No</div>
									<div class="col-md-3">Product Image</div>
									<div class="col-md-3">Product Name</div>
									<div class="col-md-3">Price in $.</div>
								</div>
							</div>
							<div class="panel-body">
								<div id="cart_product">
								
								</div>
							</div>
							<div class="panel-footer"></div>
						</div>
					</div>
				</li>
				<li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span>SignIn</a>
					<ul class="dropdown-menu">
						<div style="width:300px;">
							<div class="panel panel-primary">
								<div class="panel-heading">Login</div>
								<div class="panel-heading">
									<form onsubmit="return false" id="login">
										<label for="email">Email</label>
										<input type="email" class="form-control" name="email" id="email" required/>
										<label for="email">Password</label>
										<input type="password" class="form-control" name="password" id="password" required/>
										<p><br/></p>
										<a href="#" style="color:white; list-style:none;">Forgotten Password</a><input type="submit" class="btn btn-success" style="float:right;">
									</form>
								</div>
								<div class="panel-footer" id="e_msg"></div>
							</div>
						</div>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</div>	
	<p><br/></p>
	<p><br/></p>
	<p><br/></p>
	<p><br/></p>
	<p><br/></p>
	<p><br/></p>

	<div class="clear"> <br><br></div>
<div class="wrap">
					<div class="image-slider">
						<!-- Slideshow 1 -->
					    <ul class="rslides" id="slider1">
						<li><img src="product_images/BAN1.jpg" alt="" style="height:400px;"></li>
					    <li><img src="product_images/BAN2.jpg" alt="" style="height:400px;"></li>
					    <li><img src="product_images/BAN3.jpg" style="height:400px;" alt=""></li>
					    </ul>
						 <!-- Slideshow 2 -->
					</div>
					<!--End-image-slider---->
					</div>
	<p><br/></p>
    <div class="wrap">
	
	<div class="content">
		    	<div class="section group">				
				<div class="col span_1_of_3">
					<div class="contact_info">
			    	 	<h2>Find Us Here</h2>
			    	 		<div class="map">
                             <iframe width="100%" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265&amp;output=embed"></iframe><br><small><a href="https://maps.google.co.in/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265" style="color:#666;text-align:left;font-size:12px">View Larger Map</a></small>
                            </div>
                    </div>
      			<div class="company_address">
				     	<h2>Company Information :</h2>
						    	<p>No 80, Galle Road,</p>
						   		<p>Dr.R.D.A. Mel Mawatha,</p>
						   		<p>Colombo 06</p>
				   		<p>Phone:(00) 222 666 444</p>
				   		<p>Fax: (000) 222 666 444</p>
				 	 	<p>Email: <span><a href="mailto:yathushanrajendran@my.sliit.lk">SweetGift@mycompany.com</a></span></p>
				   		<p>Follow on: <span><a href="#">Facebook</a></span>, <span><a href="#">Twitter</a></span></p>
				   </div>
				</div>				
				<div class="col span_2_of_3">
				    <div class="contact-form">
                    <h2>Contact Us</h2>
                    <div class="row">                    
                        <div class="col-lg-4 col-md-6 mb-4">
                        <br>
                        <br>
                        <div class="card h-100">
                            <a href="#"><img class="card-img-top" src="product_images/Shanja.jfif " alt="logo"></a>
                            <div class="card-body">                            
                                <a href="#"><h3 class="text-center">Shanja Sri</a></h3>
                                <h5 class="text-center">Manager<p>0769011872</p></h5>
                                <p class="text-center">834,point pedro road nallur jaffna</p>
                            </div>
                        </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                                <a href="#"><img class="card-img-top" src="product_images/mia.jpg " alt="logo"></a>
                                <div class="card-body">
                                    <a href="#"><h3 class="text-center">Miyaa Lux</a></h3>
                                    <h5 class="text-center">Manager<p>0760000000</p></h5>
                                    <p class="text-center">nallur jaffna</p>
                                </div>
                            </div>
                        </div>
                    </div>
  				    </div>				
			    </div>
			  	<div class="clear"> 
                </div>
	        </div>
	<div class="clear"> </div>
		    </div>
		    </div>
	</div>
	<div class="footer">
			<div class="wrap">
				<div class="section group">
					<div class="col_1_of_4 span_1_of_4">
						<h3>Our Info</h3>
						<p>Sweet Choco Gift gives you a chance to quickly and easily find the phone you want and have it delivered to your home in no time, regardless of your location, as long as it is in one of the countries of the EU..</p>
					</div>
					<div class="col_1_of_4 span_1_of_4">
						<h3>WHY DO CUSTOMERS LOVE US?</h3>
						<p>We have been in the business for quite a while now, and it that time we have not only managed to make close relationships with numerous suppliers all over the world, but also to recognize what people need. This means that we are always able to offer all the latest phones, great prices, reliable service, fast delivery and premium customer support.</p>
						
					</div>
					<div class="col_1_of_4 span_1_of_4">
						<h3>Store Location</h3>
						<p>No 80, Galle Road, Dr.R.D.A. Mel Mawatha, Colombo 06</p>
						<h3>Order-online</h3>
						<p>Phone:(+94) 77 546 2455</p>
						<p>Fax: (+21) 222 666 4445</p>
					</div>
					<div class="col_1_of_4 span_1_of_4 footer-lastgrid">
						<h3>News-Letter</h3>
						<form>
							<input type="text"><input type="submit" value="go" />
						</form>
						<h3>Follow Us:</h3>
						<ul>
							<li><a href="#"><img src="product_images/twitter.png" title="twitter" />Twitter</a></li>
							<li><a href="#"><img src="product_images/facebook.png" title="Facebook" />Facebook</a></li>
							<li><a href="#"><img src="product_images/rss.png" title="Rss" />Rss</a></li>
						</ul>
					</div>
				</div>
			</div>
		<div class="clear"> </div>
		<div class="wrap">
			<div class="copy-right">
			<p>&copy; 2019 Sweet Choco Gift. All Rights Reserved.</p>
			</div>
		</div>
	</div>
	
</body>
</html>

















































-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 25, 2019 at 04:10 PM
-- Server version: 5.7.24
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobileworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
CREATE TABLE IF NOT EXISTS `cart` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `p_id` int(10) NOT NULL,
  `ip_add` varchar(250) NOT NULL,
  `user_id` int(10) DEFAULT NULL,
  `qty` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `p_id`, `ip_add`, `user_id`, `qty`) VALUES
(2, 1, '::1', 3, 1),
(3, 2, '::1', 3, 1),
(5, 4, '::1', 3, 1),
(6, 1, '::1', -1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `cat_id` int(100) NOT NULL AUTO_INCREMENT,
  `cat_title` text NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(1, 'Basket'),
(2, 'Bouquet'),
(3, 'Gift Hamper'),
(4, 'Birthday Gift'),
(5, 'Chocolate & Flower'),
(6, 'Valentine Gift'),
(7, 'Gift');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `trx_id` varchar(255) NOT NULL,
  `p_status` varchar(20) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `product_id`, `qty`, `trx_id`, `p_status`) VALUES
(1, 2, 7, 1, '07M47684BS5725041', 'Completed'),
(2, 2, 2, 1, '07M47684BS5725041', 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(100) NOT NULL AUTO_INCREMENT,
  `product_cat` int(100) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_price` int(100) NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` text NOT NULL,
  `product_keywords` text NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_cat`, `product_title`, `product_price`, `product_desc`, `product_image`, `product_keywords`) VALUES
(1, 1, 'Dark Chocolate Basket', 5000, 'Dark Chocolate Basket', '1.jpg', 'chocolate basket'),
(2, 1, 'Chocolate Basket',4900, 'Chocolate Basket\variety chocolate', '2.jpg', 'chocolate basket'),
(3, 1, 'Diary Chocolate Basket', 10000, 'Dairy Chocolate', '3.jpg', 'Dairy Chocolate Basket'),
(4, 1, 'Gold Chocolate Basket', 65000, 'Gold/chocolate/Basket', '4.jpg', 'Golden chocolate Basket'),
(5, 1, 'Colorful choco Basket', 2000, 'choco basket', '5.jpg', 'Choco Basket'),
(6, 1, 'Toy Choco Basket', 3500, 'Dairy Chocolate Basket with Toys', '6.jpg', 'Toy\choco basket'),

(7, 2, '5 star Chocolate Bouquet', 12000, 'Dairy and 5 star chrunchy chocloate Bouquet', '21.jpg', '5 star\Dairy\Bouquet'),
(8, 2, 'Boost Chocolate Bouquet', 8000, 'Purple Chocolate Bouquet', '22.jpg', 'Purple\Chocolate Bouquet'),
(9, 2, 'Rose Chocolate Bouquet', 10000, 'Dairy Chocolate Bouquet with roses', '23.jpg', 'Dairy/Rose'),

(10, 3, 'Gift Hamper', 7000, 'Snickers Gift Hanper', '31.jpg', 'Chocolate Gift Hamper'),

(11, 4, 'Birthday Gift Basket', 10000, 'White chocolate Birthday Gift Basket', '41.jpg', 'Birthday Gift'),
(12, 4, 'Red Bouquet', 3000, 'Dairy Chocolate\Red Rose', '42.jpg', 'Red Rose chocolate Bouquet'),
(13, 4, 'Bithday Bouquet', 9500, 'Birthday Bouquet', '43.jpg', 'Chocolate Birthday Bouquet'),
(14, 4, 'Birthday Surprise Bouquet', 5000, 'Birthday Bouquet Red decoration', '44.jpg', 'Birthday surpirse Bouquet'),
(15, 4, 'Dairy Chocolate Bouquet', 7000, 'Birthday gift Bouquet', '46.jpg', 'Birthday Gift Bouquet'),
(16, 4, 'Gift Box', 10000, 'Dark chocolate gift box', '47.jpg', 'Dark chocolate\Gift'),
(17, 4, 'Mom Gift Box', 6000, 'Chocolate Box for Mom Bithday', '49.jpg', 'Mom\Birthday\Gift'),
(18, 4, 'Luxury Chocolate', 15000, 'Luxury chocolate for Birthday gift', '48.jpg', 'Birthday Gift'),
(19, 4, 'Special Birthday gift Basket', 20000, 'Birthday Gift Basket for special person', '45.jpg', 'special gift'),

(20, 5, 'Rose Bouquet', 16000, 'Dairy chocolate with Rose Flower Bouquet', '51.jpg', 'Rose Bouquet'),

(25, 6, 'Valentine Gift Bouquet', 8000, 'Valentine special Bouquet', '61.jpg', 'valentine Bouquet'),
(26, 6, 'Three Layer Chocolate Gift', 25000, 'Love Gift', '65.jpg', 'valentine special'),


(27, 7, 'Chocolate Heart', 12000, 'Heartful Gift', '71.jpg', 'special gift');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
CREATE TABLE IF NOT EXISTS `user_info` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `address1` varchar(300) NOT NULL,
  `address2` varchar(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `address1`, `address2`) VALUES
(3, 'yathushan', 'shan', 'rajendranyathushan3@gmail.com', '$2y$10$rzReANcv9T9f1JP8rYbT1eW31cOCu3253vEG84uBmBL/hpqEt75Vy', '0774579991', 'jaffna', 'jaffna');
COMMIT;

